<%

//Scriptlet tag is used to perform Java source code

	String contextPath = request.getContextPath();
	String title = "Todo List App";
	String message = "Welcome ";
%>

<%
	//Expression tag used to print values of variables
%>

<%!
	//field or method declarations	
	public String getDate(){
		return java.util.Calendar.
				getInstance().getTime().toString();
	}
%>
<head><title><%= title %></title></head>
<body>
<h2><%= message %></h2>

<br/>
<h3></h3>
<ul>
	<li>---------------------------------</li>
	<li><a href="<%= contextPath %>
		/views/MainMenu.jsp">Go To My To-Do List</a></li>	
</ul>
<br/>

</body>
</html>
